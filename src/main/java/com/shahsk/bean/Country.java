package com.shahsk.bean;

public class Country {

    private int countryId;
    private String courntyName;
    private String counrtyCode;

    public Country(int countryId, String courntyName, String counrtyCode) {
        this.countryId = countryId;
        this.courntyName = courntyName;
        this.counrtyCode = counrtyCode;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getCourntyName() {
        return courntyName;
    }

    public void setCourntyName(String courntyName) {
        this.courntyName = courntyName;
    }

    public String getCounrtyCode() {
        return counrtyCode;
    }

    public void setCounrtyCode(String counrtyCode) {
        this.counrtyCode = counrtyCode;
    }

    @Override
    public String toString() {
        return "[" + getCountryId() + ", " + getCourntyName() + ", " + getCounrtyCode() + "]";
    }
}
