package com.shahsk.utils;

import com.shahsk.bean.Country;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class SimpleParser {

    public static ArrayList<Country> parse(String file) throws IOException {
        ArrayList<Country> countries = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line = "";
        while ((line = br.readLine()) != null) {
            String[] val = line.split(",");
            Country c = new Country(Integer.parseInt(val[0]),val[1],val[2]);
            countries.add(c);
        }

        br.close();
        return countries;
    }
}
