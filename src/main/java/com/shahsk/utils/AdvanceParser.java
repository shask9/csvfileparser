package com.shahsk.utils;

import com.shahsk.bean.Country;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class AdvanceParser {

    private static final char DEFAULT_SEPERATOR = ',';
    private static final char DEFAULT_QUOTE = '"';

    private static BufferedReader br;

    public static ArrayList<Country> parse(String file) throws IOException {
        ArrayList<Country> countries = new ArrayList<>();
        br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            ArrayList<String> values = parseLine(line, DEFAULT_SEPERATOR, DEFAULT_QUOTE);
            System.out.println(values);
            Country c = new Country(Integer.parseInt(values.get(0)), values.get(1), values.get(2));
            countries.add(c);
        }

        br.close();
        return countries;

    }

    public static void parse(String file, char seperator) throws IOException {
        br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null)
            parseLine(line, seperator, DEFAULT_QUOTE);
    }

    private static ArrayList<String> parseLine(String line, char separator, char quote) {

        boolean insideQuotes = false;
        ArrayList<String> values = new ArrayList<>();
        char[] chars = line.toCharArray();
        StringBuffer val = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];

            if (insideQuotes) {

                if (ch == separator)
                    continue;

                if (ch == quote) {
                    if ((i+1)!= chars.length && chars[i+1] == separator)
                        insideQuotes = false;

                    continue;
                }

                val.append(ch);
            } else {
                if (ch == separator) {
                    values.add(val.toString());
                    val = new StringBuffer();
                    continue;
                }

                if((i+1)==chars.length) {
                    val.append(ch);
                    values.add(val.toString());
                    continue;
                }

                if (ch == quote) {
                    insideQuotes = true;
                    continue;
                }

                val.append(ch);
            }
        }
        return values;
    }
}
