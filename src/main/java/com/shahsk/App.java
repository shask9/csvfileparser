package com.shahsk;

import com.shahsk.bean.Country;
import com.shahsk.utils.AdvanceParser;
import com.shahsk.utils.SimpleParser;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) {
        String file = App.class.getClassLoader().getResource("input.csv").getFile();
        ArrayList<Country> countries = new ArrayList<>();
        try {
            countries = AdvanceParser.parse(file);
        } catch (IOException ie) {
            ie.printStackTrace();
        }

        System.out.println(countries);
    }
}
